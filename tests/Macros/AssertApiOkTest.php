<?php

namespace Macros;

use Altra\Responses\Tests\TestCase;
use Illuminate\Testing\TestResponse;

class AssertApiOkTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_if_exists_macro()
    {
        $this->assertTrue(TestResponse::hasMacro('assertApiOk'));
    }
}
