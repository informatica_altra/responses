<?php

namespace Macros;

use Altra\Responses\Tests\TestCase;
use Illuminate\Testing\TestResponse;

class AssertApiHasWarningsTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_if_exists_macro_assert_api_has_warning()
    {
        $this->assertTrue(TestResponse::hasMacro('assertApiHasWarning'));
    }

    public function test_if_exists_macro_assert_api_has_warnings()
    {
        $this->assertTrue(TestResponse::hasMacro('assertApiHasWarnings'));
    }
}
