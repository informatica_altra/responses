<?php

namespace Macros;

use Altra\Responses\Tests\TestCase;
use Illuminate\Testing\TestResponse;

class AssertApiErrorTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_if_exists_macro_assert_api_error()
    {
        $this->assertTrue(TestResponse::hasMacro('assertApiError'));
    }

    public function test_if_exists_macro_assert_api_error_message()
    {
        $this->assertTrue(TestResponse::hasMacro('assertApiErrorMessage'));
    }
}
