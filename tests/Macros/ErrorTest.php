<?php

namespace Macros;

use Altra\Responses\Services\WarningService;
use Altra\Responses\Tests\TestCase;
use Illuminate\Support\Facades\Response;

class ErrorTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_if_exists_macro()
    {
        $this->assertTrue(Response::hasMacro('error'));
    }

    public function test_error_response_without_message()
    {
        $response = response()->error([], 500, []);
        $this->assertEquals(
            json_encode(['result' => 'ERRORS', 'message' => [], 'errors' => []]),
            json_encode($response->original)
        );
    }

    public function test_error_response_with_message()
    {
        WarningService::fakeWarning('E000003', ['code' => 'E000003', 'message' => 'Mensaje de error']);
        $response = response()->error([], 500, [['code' => 'E000003']]);
        $this->assertEquals(
            json_encode(['result' => 'ERRORS', 'message' => [], 'errors' => [['code' => 'E000003', 'message' => 'Mensaje de error']]]),
            json_encode($response->original)
        );
    }

    public function test_validate_structure()
    {
        $message = 'Ha habido un error';
        WarningService::fakeWarning('E000003', ['code' => 'E000003', 'message' => 'Mensaje de error']);
        $response = response()->error(compact('message'), 500, [['code' => 'E000003']]);
        $this->assertEquals(
            json_encode(['result' => 'ERRORS', 'message' => ['message' => $message], 'errors' => [['code' => 'E000003', 'message' => 'Mensaje de error']]]),
            json_encode($response->original)
        );
    }
}
