<?php

use Altra\Responses\Services\WarningService;
use Altra\Responses\Tests\TestCase;

class HelperTest extends TestCase
{
    public function test_code_does_not_exists()
    {
        WarningService::fakeWarning('code_that_does_not_exist', null);
        $codeAndMessage = getErrorMessagesByCode([['code' => 'code_that_does_not_exist']]);
        $this->assertIsObject($codeAndMessage);
        $this->assertEquals(collect(), $codeAndMessage);
    }

    public function test_helper_get_messages()
    {
        WarningService::fakeWarning('W000001', ['code' => 'W000001', 'message' => 'Mensaje de warning']);
        $codeAndMessage = getErrorMessagesByCode([['code' => 'W000001']]);
        $this->assertEquals(json_encode($codeAndMessage), json_encode([['code' => 'W000001', 'message' => 'Mensaje de warning']]));
    }
}
