<?php

namespace Altra\Responses\Tests;

use Altra\Responses\MacroServiceProvider;
use Orchestra\Testbench\TestCase as Orchestra;

class TestCase extends Orchestra
{
    /**
     * @throws \ReflectionException
     */
    protected function setUp(): void
    {
        parent::setUp();
        config(['responses.results.success' => 'SUCCESS']);
        config(['responses.results.warnings' => 'WARNINGS']);
        config(['responses.results.errors' => 'ERRORS']);
        config(['responses.apikey' => null]);
        config(['responses.warnings_url' => 'test-endpoint']);
        $this->createDummyprovider()->register();
        $this->createDummyprovider()->boot();
    }

    /**
     * @throws \ReflectionException
     */
    protected function createDummyprovider(): object
    {
        $reflectionClass = new \ReflectionClass(MacroServiceProvider::class);

        return $reflectionClass->newInstanceWithoutConstructor();
    }
}
