<?php

return [
    // Kong apikey
    'apikey' => null,
    'warnings_url' => null,

    // API results
    'results' => [
        'success' => 'SUCCESS',
        'warnings' => 'WARNINGS',
        'errors' => 'ERRORS',
    ]
];
