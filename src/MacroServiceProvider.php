<?php

namespace Altra\Responses;

use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;

class MacroServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(): void
    {
        $this->publishes([__DIR__.'/../config/responses.php' => config_path('responses.php')]);
        $this->macros()->each(fn ($class) => app($class)());
    }

    private function macros(): Collection
    {
        return collect([
            'ok' => \Altra\Responses\Macros\Ok::class,
            'error' => \Altra\Responses\Macros\Error::class,
            'assertApiOk' => \Altra\Responses\Macros\AssertApiOk::class,
            'assertApiError' => \Altra\Responses\Macros\AssertApiError::class,
            'assertApiHasWarnings' => \Altra\Responses\Macros\AssertApiHasWarnings::class,
        ]);
    }
}
