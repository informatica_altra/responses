<?php

use Altra\Responses\Services\WarningService;

if (! function_exists('getErrorMessagesByCode')) {

    /**
     * Get error|code messages via configuration file codes
     *
     * @param  array  $codes
     * @return object|array messages
     */
    function getErrorMessagesByCode(array $codes = []): object|array
    {
        $messages = collect();

        foreach ($codes as $code) {
            if (empty($code['code'])) {
                return $codes;
            }
            $warning = app(WarningService::class)->getByCode($code);
            if ($warning != null) {
                $messages->push($warning);
            }
        }

        return ! empty($messages) ? $messages : [];
    }
}
