<?php

namespace Altra\Responses\Macros;

use Illuminate\Testing\Assert;
use Illuminate\Testing\TestResponse;

class AssertApiHasWarnings
{
    public function __invoke()
    {
        TestResponse::macro('assertApiHasWarnings', function () {
            Assert::assertEquals('WARNING', $this->baseResponse->getData()->result, 'Result must be WARNING!');
            Assert::assertGreaterThan(0, count($this->baseResponse->getData()->warnings ?? []), 'Response must have at least 1 warning');
        });

        TestResponse::macro('assertApiHasWarning', function (array $warning) {
            Assert::assertEquals('WARNING', $this->baseResponse->getData()->result, 'Result must be WARNING!');
            Assert::assertContains($warning, $this->baseResponse->getData()->warnings ?? [], 'Warnings array must have '.json_encode($warning));
        });
    }
}
