<?php

namespace Altra\Responses\Macros;

use Illuminate\Testing\Assert;
use Illuminate\Testing\TestResponse;

class AssertApiOk
{
    public function __invoke()
    {
        TestResponse::macro('assertApiOk', function () {
            Assert::assertGreaterThanOrEqual(200, $this->baseResponse->status());
            Assert::assertLessThan(300, $this->baseResponse->status());
            Assert::assertContains($this->baseResponse->getData()->result, ['WARNING', 'SUCCESS'], 'Result must be WARNING or SUCCESS!');
        });
    }
}
