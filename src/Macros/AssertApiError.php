<?php

namespace Altra\Responses\Macros;

use Illuminate\Testing\Assert;
use Illuminate\Testing\TestResponse;

class AssertApiError
{
    public function __invoke()
    {
        TestResponse::macro('assertApiError', function () {
            Assert::assertGreaterThanOrEqual(400, $this->baseResponse->status());
            Assert::assertEquals('ERRORS', $this->baseResponse->getData()->result, 'Result must be ERRORS!');
            $this->assertJsonStructure(['result', 'message', 'errors']);
        });

        TestResponse::macro('assertApiErrorMessage', function (string $message) {
            Assert::assertEquals($message, $this->baseResponse->getData()->message, 'Error Message must be exactly '.$message);
        });
    }
}
