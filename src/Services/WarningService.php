<?php

namespace Altra\Responses\Services;

use Illuminate\Support\Facades\Http;

class WarningService
{
    public function getByCode(array $warning)
    {
      $endpoint = static::getWarningEndpoint($warning['code']);
      $response = Http::withHeaders(['apikey' => config('responses.apikey')])->get($endpoint)->json();
      $response = $response != null ? $response['body']['warning'] : null;
      if ($response != null) {
        $response = ['code' => $response['code'], 'message' => __($response['message'], $warning['vars'] ?? [])];
      }
      return $response;
    }

    public static function fakeWarning($code, $response)
    {
      $endpoint = static::getWarningEndpoint($code);
      Http::fake([
        $endpoint => Http::response(['body' => ['warning' => $response]], 200),
      ]);
    }

    private static function getWarningEndpoint($code)
    {
      return __(config('responses.warnings_url'), ['CODE' => $code]);
    }
}
